<style>
    
        /* .cz-countdown-days {
            color: white !important;
            background-color: {{ $web_config['primary_color'] }};
            padding: 0px 6px;
            border-radius: 3px;
            margin-right: 3px !important;
        }

        .cz-countdown-hours {
            color: white !important;
            background-color: {{ $web_config['primary_color'] }};
            padding: 0px 6px;
            border-radius: 3px;
            margin-right: 3px !important;
        }

        .discount-top-f {
            text-align: end;
           
            margin-bottom: 5px;
        }

        .cz-countdown-minutes {
            color: white !important;
            background-color: {{ $web_config['primary_color'] }};
            padding: 0px 6px;
            border-radius: 3px;
            margin-right: 3px !important;
        }

        .cz-countdown-seconds {
            color: {{ $web_config['primary_color'] }};
            border: 1px solid{{ $web_config['primary_color'] }};
            padding: 0px 6px;
            border-radius: 3px !important;
        }

        .flash_deal_product_details .flash-product-price {
            font-weight: 700;
            font-size: 18px;
            color: {{ $web_config['primary_color'] }};
        }

        .for-discoutn-value {
            background: {{ $web_config['primary_color'] }};

        }

        .featured_deal_left {
            height: 130px;
            background: {{ $web_config['primary_color'] }} 0% 0% no-repeat padding-box;
            padding: 10px 100px;
            text-align: center;
        }

        .featured_deal {
            min-height: 130px;

        }

        .category_div:hover {
            color: {{ $web_config['secondary_color'] }};
        }

        .deal_of_the_day {
            
            opacity: .8;
            background: {{ $web_config['secondary_color'] }};
            border-radius: 3px;
        }

        .deal-title {
            font-size: 12px;

        }

        .for-flash-deal-img img {
            max-width: none;
        }

        @media (max-width: 375px) {
            .cz-countdown {
                display: flex !important;

            }

            .cz-countdown .cz-countdown-seconds {

                margin-top: -5px !important;
            }

            .for-feature-title {
                font-size: 20px !important;
            }
        }

        @media (max-width: 600px) {
            .flash_deal_title {
                font-weight: 600;
                font-size: 18px;
                text-transform: uppercase;
            }

            .cz-countdown .cz-countdown-value {
                font-family: "Roboto", sans-serif;
                font-size: 11px !important;
                font-weight: 700 !important;
            }

            .featured_deal {
                opacity: 1 !important;
            }

            .cz-countdown {
                display: inline-block;
                flex-wrap: wrap;
                font-weight: normal;
                margin-top: 4px;
                font-size: smaller;
            }

            .view-btn-div-f {

                margin-top: 6px;
                float: right;
            }

            .view-btn-div {
                float: right;
            }

            .viw-btn-a {
                font-size: 10px;
                font-weight: 600;
            }


            .for-mobile {
                display: none;
            }

            .featured_for_mobile {
                max-width: 95%;
                margin-top: 20px;
            }
        }

        @media (max-width: 360px) {
            .featured_for_mobile {
                max-width: 96%;
                margin-top: 11px;
            }

            .featured_deal {
                opacity: 1 !important;
            }
        }

        @media (max-width: 375px) {
            .featured_for_mobile {
                max-width: 96%;
                margin-top: 11px;
            }

            .featured_deal {
                opacity: 1 !important;
            }

            .for-iphone-mobile {
                margin-left: 2%;
            }
        }

        @media (min-width: 768px) {
            .displayTab {
                display: block !important;
            }
        }

        @media (max-width: 800px) {
            .for-tab-view-img {
                width: 40%;
            }

            .for-tab-view-img {
                width: 105px;
            }

            .widget-title {
                font-size: 19px !important;
            }
        }

        .featured_deal_carosel .carousel-inner {
            width: 100% !important;
        } */

        .stock-out {
        position: absolute;
        left: 3% !important;
        top: 3% !important;
        color: white !important;
        font-weight: 900;
        font-size: 15px;
    }
    </style>
    

    {{-- .product-card {
        height: 100%;
    }

    .badge-style {
        right: 0 !important;
        left: 75% !important;
        margin-top: -2px !important;
        background: transparent !important;
        color: black !important;
    }  --}}



    @php($overallRating = \App\CPU\ProductManager::get_overall_rating($product->reviews))
    
        
    <div class="product-wrap product text-center">
        <div class="product-card card {{$product['current_stock']==0?'stock-card':''}}">
            @if ($product['current_stock'] <= 0)
            <label class="badge badge-danger stock-out">Stock Out</label>
        @endif
    @if ($product->discount > 0)
            <div class="d-flex justify-content-end for-dicount-div discount-hed">
                    <span class="for-discoutn-value">
                    @if ($product->discount_type == 'percent')
                            {{round($product->discount,2)}}%
                        @elseif($product->discount_type =='flat')
                            {{\App\CPU\Helpers::currency_converter($product->discount)}}
                        @endif
                        OFF
                    </span>
            </div>
        @else
            <div class="d-flex justify-content-end for-dicount-div-null">
                <span class="for-discoutn-value-null"></span>
            </div>
        @endif
        
        <figure class="product-media">
            <a href="productfashion.html">
                <img src="assets/images/watch.jpg" alt="Product" width="216" height="243">
            </a>
           
        </figure>
        
        <div class="product-details">
            <h4 class="product-name"><a href="{{route('product',$product->slug)}}">{{ Str::limit($product['name'], 30) }}</a>
            </h4>
            <div class="ratings-container">
                {{-- <div class="ratings-full">
                    <span class="ratings" style="width: 60%;"></span>
                    <span class="tooltiptext tooltip-top"></span>
                </div> --}}
                @for ($inc = 0; $inc < 5; $inc++)
                    @if ($inc < $overallRating[0])
                        <i class="sr-star czi-star-filled active"></i>
                    @else
                        <i class="sr-star czi-star"></i>
                    @endif
                @endfor
                <a href="product-default.html" class="rating-reviews">( {{$product->reviews()->count()}} )</a>
            </div>
            <div class="product-price">
                @if ($product->discount > 0)
                <del class="old-price">
                    {{\App\CPU\Helpers::currency_converter($product->unit_price)}}
                </del>
            @endif
            
            <ins class="new-price">{{\App\CPU\Helpers::currency_converter(
                $product->unit_price-(\App\CPU\Helpers::get_product_discount($product,$product->unit_price))
            )}}</ins>
            </div>
        </div>
    </div>
</div>
    
{{-- @php($overallRating = \App\CPU\ProductManager::get_overall_rating($product->reviews))

     
<div class="product-wrap product text-center{{$product['current_stock']==0?'stock-card':''}}"
    style="margin-bottom: 40px">
    @if ($product['current_stock'] <= 0)
        <label class="badge badge-danger stock-out">Stock Out</label>
    @endif
    
        @if ($product->discount > 0)
            <div class="d-flex justify-content-end for-dicount-div discount-hed">
                    <span class="for-discoutn-value">
                    @if ($product->discount_type == 'percent')
                            {{round($product->discount,2)}}%
                        @elseif($product->discount_type =='flat')
                            {{\App\CPU\Helpers::currency_converter($product->discount)}}
                        @endif
                        OFF
                    </span>
            </div>
        @else
            <div class="d-flex justify-content-end for-dicount-div-null">
                <span class="for-discoutn-value-null"></span>
            </div>
        @endif
        
    
    <figure class="product-media">
        <a href="productelectronics.html">
            <a href="{{route('product',$product->slug)}}">
                <img src="assets/images/list-3.jpg">
            {{-- <img src="{{\App\CPU\ProductManager::product_image_path('thumbnail')}}/{{$product['thumbnail']}}"
            onerror="this.src='{{asset('public/assets/front-end/img/image-place-holder.png')}}'" alt="Product" width="216" height="243"> 
        </a>

    </figure>
    <div class="product-details">
        
        <h4 class="product-name"><a href="{{route('product',$product->slug)}}">{{ Str::limit($product['name'], 30) }}</a>
        </h4>
        {{-- @if (Request::is('product/*'))
                <a class="btn btn-primary btn-sm btn-block mb-2" href="{{route('product',$product->slug)}}">
                    <i class="czi-forward align-middle mr-1"></i>
                    {{trans('messages.View')}}
                </a>
            @else
                <a class="btn btn-primary btn-sm btn-block mb-2" href="javascript:"
                   onclick="quickView('{{$product->id}}')">
                    <i class="czi-eye align-middle mr-1"></i>
                    {{trans('messages.Quick')}}   {{trans('messages.View')}}
                </a>
            @endif 
        <div class="ratings-container">
            <div class="ratings-full">
                <span class="ratings" style="width: 60%;"></span>
                <span class="tooltiptext tooltip-top"></span>
            </div>
            <a href="product-default.html" class="rating-reviews">(3 reviews)</a>
        </div>
        <div class="product-price">
            <ins class="new-price">$23.99</ins><del class="old-price">$25.68</del>
        </div>
    </div>
</div> --}}






{{-- @php($overallRating = \App\CPU\ProductManager::get_overall_rating($product->reviews))

<div class="product-card card {{$product['current_stock']==0?'stock-card':''}}"
     style="margin-bottom: 40px">
    @if ($product['current_stock'] <= 0)
        <label class="badge badge-danger stock-out">Stock Out</label>
    @endif

    <div class="card-header inline_product clickable" style="cursor: pointer;max-height: 193px;min-height: 193px">
        @if ($product->discount > 0)
            <div class="d-flex justify-content-end for-dicount-div discount-hed">
                    <span class="for-discoutn-value">
                    @if ($product->discount_type == 'percent')
                            {{round($product->discount,2)}}%
                        @elseif($product->discount_type =='flat')
                            {{\App\CPU\Helpers::currency_converter($product->discount)}}
                        @endif
                        OFF
                    </span>
            </div>
        @else
            <div class="d-flex justify-content-end for-dicount-div-null">
                <span class="for-discoutn-value-null"></span>
            </div>
        @endif
        <div class="d-flex align-items-center justify-content-center d-block">
            <a href="{{route('product',$product->slug)}}">
                <img src="{{\App\CPU\ProductManager::product_image_path('thumbnail')}}/{{$product['thumbnail']}}"
                     onerror="this.src='{{asset('public/assets/front-end/img/image-place-holder.png')}}'"
                     style="width: 100%">
            </a>
        </div>
    </div>

    <div class="card-body inline_product text-center p-1 clickable"
         style="cursor: pointer; height:5.5rem; max-height: 5.5rem">
        <div class="rating-show">
            <span class="d-inline-block font-size-sm text-body">
                @for ($inc = 0; $inc < 5; $inc++)
                    @if ($inc < $overallRating[0])
                        <i class="sr-star czi-star-filled active"></i>
                    @else
                        <i class="sr-star czi-star"></i>
                    @endif
                @endfor
                <label class="badge-style">( {{$product->reviews()->count()}} )</label>
            </span>
        </div>
        <div style="position: relative;" class="product-title1">
            <a href="{{route('product',$product->slug)}}">
                {{ Str::limit($product['name'], 30) }}
            </a>
        </div>
        <div class="justify-content-between text-center">
            <div class="product-price text-center">
                @if ($product->discount > 0)
                    <strike style="font-size: 12px!important;color: grey!important;">
                        {{\App\CPU\Helpers::currency_converter($product->unit_price)}}
                    </strike><br>
                @endif
                <span class="text-accent">
                    {{\App\CPU\Helpers::currency_converter(
                        $product->unit_price-(\App\CPU\Helpers::get_product_discount($product,$product->unit_price))
                    )}}
                </span>
            </div>
        </div>
    </div>

    <div class="card-body card-body-hidden" style="padding-bottom: 5px!important;">
        <div class="text-center">
            @if (Request::is('product/*'))
                <a class="btn btn-primary btn-sm btn-block mb-2" href="{{route('product',$product->slug)}}">
                    <i class="czi-forward align-middle mr-1"></i>
                    {{trans('messages.View')}}
                </a>
            @else
                <a class="btn btn-primary btn-sm btn-block mb-2" href="javascript:"
                   onclick="quickView('{{$product->id}}')">
                    <i class="czi-eye align-middle mr-1"></i>
                    {{trans('messages.Quick')}}   {{trans('messages.View')}}
                </a>
            @endif
        </div>
    </div>
</div> --}}
