@extends('layouts.front-end.app')

@section('title', 'Welcome To ' . $web_config['name']->value . ' Home')

@push('css_or_js')
    <meta property="og:image"
        content="{{ asset('storage/app/public/company') }}/{{ $web_config['web_logo']->value }}" />
    <meta property="og:title" content="Welcome To {{ $web_config['name']->value }} Home" />
    <meta property="og:url" content="{{ env('APP_URL') }}">
    <meta property="og:description" content="{!! substr($web_config['about']->value, 0, 100) !!}">

    <meta property="twitter:card"
        content="{{ asset('storage/app/public/company') }}/{{ $web_config['web_logo']->value }}" />
    <meta property="twitter:title" content="Welcome To {{ $web_config['name']->value }} Home" />
    <meta property="twitter:url" content="{{ env('APP_URL') }}">
    <meta property="twitter:description" content="{!! substr($web_config['about']->value, 0, 100) !!}">

    <link rel="stylesheet" href="{{ asset('public/assets/front-end') }}/css/home.css" />
    <link rel="preload" href="assets/vendor/fontawesome-free/webfonts/fa-regular-400.woff2" as="font" type="font/woff2"
        crossorigin="anonymous">
    <link rel="preload" href="assets/vendor/fontawesome-free/webfonts/fa-solid-900.woff2" as="font" type="font/woff2"
        crossorigin="anonymous">
    <link rel="preload" href="assets/vendor/fontawesome-free/webfonts/fa-brands-400.woff2" as="font" type="font/woff2"
        crossorigin="anonymous">


    <!-- Vendor CSS -->
    <link rel="stylesheet" type="text/css" href="assets/vendor/fontawesome-free/css/all.min.css">

    <!-- Plugins CSS -->
    <link rel="stylesheet" type="text/css" href="assets/vendor/owl-carousel/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="assets/vendor/animate/animate.min.css">
    <link rel="stylesheet" type="text/css" href="assets/vendor/magnific-popup/magnific-popup.min.css">

    <!-- Default CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/demo9.min.css">
    <script>
        WebFontConfig = {
            google: {
                families: ['Poppins:400,500,600,700,800', 'Arizonia:400']
            }
        };
        (function(d) {
            var wf = d.createElement('script'),
                s = d.scripts[0];
            wf.src = 'assets/js/webfont.js';
            wf.async = true;
            s.parentNode.insertBefore(wf, s);
        })(document);
        // .cz-countdown-days {
        //     color: white !important;
        //     background-color: {{ $web_config['primary_color'] }};
        //     padding: 0px 6px;
        //     border-radius: 3px;
        //     margin-right: 3px !important;
        // }

        // .cz-countdown-hours {
        //     color: white !important;
        //     background-color: {{ $web_config['primary_color'] }};
        //     padding: 0px 6px;
        //     border-radius: 3px;
        //     margin-right: 3px !important;
        // }

        // .discount-top-f {
        //     text-align: end;
        //     /* margin-top: 5px; */
        //     margin-bottom: 5px;
        // }

        // .cz-countdown-minutes {
        //     color: white !important;
        //     background-color: {{ $web_config['primary_color'] }};
        //     padding: 0px 6px;
        //     border-radius: 3px;
        //     margin-right: 3px !important;
        // }

        // .cz-countdown-seconds {
        //     color: {{ $web_config['primary_color'] }};
        //     border: 1px solid{{ $web_config['primary_color'] }};
        //     padding: 0px 6px;
        //     border-radius: 3px !important;
        // }

        // .flash_deal_product_details .flash-product-price {
        //     font-weight: 700;
        //     font-size: 18px;
        //     color: {{ $web_config['primary_color'] }};
        // }

        // .for-discoutn-value {
        //     background: {{ $web_config['primary_color'] }};

        // }

        // .featured_deal_left {
        //     height: 130px;
        //     background: {{ $web_config['primary_color'] }} 0% 0% no-repeat padding-box;
        //     padding: 10px 100px;
        //     text-align: center;
        // }

        // .featured_deal {
        //     min-height: 130px;

        // }

        // .category_div:hover {
        //     color: {{ $web_config['secondary_color'] }};
        // }

        // .deal_of_the_day {
        //     /* filter: grayscale(0.5); */
        //     opacity: .8;
        //     background: {{ $web_config['secondary_color'] }};
        //     border-radius: 3px;
        // }

        // .deal-title {
        //     font-size: 12px;

        // }

        // .for-flash-deal-img img {
        //     max-width: none;
        // }

        // @media (max-width: 375px) {
        //     .cz-countdown {
        //         display: flex !important;

        //     }

        //     .cz-countdown .cz-countdown-seconds {

        //         margin-top: -5px !important;
        //     }

        //     .for-feature-title {
        //         font-size: 20px !important;
        //     }
        // }

        // @media (max-width: 600px) {
        //     .flash_deal_title {
        //         font-weight: 600;
        //         font-size: 18px;
        //         text-transform: uppercase;
        //     }

        //     .cz-countdown .cz-countdown-value {
        //         font-family: "Roboto", sans-serif;
        //         font-size: 11px !important;
        //         font-weight: 700 !important;
        //     }

        //     .featured_deal {
        //         opacity: 1 !important;
        //     }

        //     .cz-countdown {
        //         display: inline-block;
        //         flex-wrap: wrap;
        //         font-weight: normal;
        //         margin-top: 4px;
        //         font-size: smaller;
        //     }

        //     .view-btn-div-f {

        //         margin-top: 6px;
        //         float: right;
        //     }

        //     .view-btn-div {
        //         float: right;
        //     }

        //     .viw-btn-a {
        //         font-size: 10px;
        //         font-weight: 600;
        //     }


        //     .for-mobile {
        //         display: none;
        //     }

        //     .featured_for_mobile {
        //         max-width: 95%;
        //         margin-top: 20px;
        //     }
        // }

        // @media (max-width: 360px) {
        //     .featured_for_mobile {
        //         max-width: 96%;
        //         margin-top: 11px;
        //     }

        //     .featured_deal {
        //         opacity: 1 !important;
        //     }
        // }

        // @media (max-width: 375px) {
        //     .featured_for_mobile {
        //         max-width: 96%;
        //         margin-top: 11px;
        //     }

        //     .featured_deal {
        //         opacity: 1 !important;
        //     }

        //     .for-iphone-mobile {
        //         margin-left: 2%;
        //     }
        // }

        // @media (min-width: 768px) {
        //     .displayTab {
        //         display: block !important;
        //     }
        // }

        // @media (max-width: 800px) {
        //     .for-tab-view-img {
        //         width: 40%;
        //     }

        //     .for-tab-view-img {
        //         width: 105px;
        //     }

        //     .widget-title {
        //         font-size: 19px !important;
        //     }
        // }

        // .featured_deal_carosel .carousel-inner {
        //     width: 100% !important;
        // }
    </script>
@endpush

@section('content')
    <div class="home" style="background-color: #fbfdfd;">
        <main class="main">
            <!-- Hero (Banners + Slider)-->
            <section class="bg-transparent">
                <div class="row ">
                    <div class="col-12">
                        <div class="banner_card">
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    @php($main_banner = \App\Model\Banner::where('banner_type', 'Main Banner')->where('published', 1)->orderBy('id', 'desc')->get())
                                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                        <ol class="carousel-indicators">
                                            @foreach ($main_banner as $key => $banner)
                                                <li data-target="#carouselExampleIndicators"
                                                    data-slide-to="{{ $key }}"
                                                    class="{{ $key == 0 ? 'active' : '' }}">
                                                </li>
                                            @endforeach
                                        </ol>
                                        <div class="carousel-inner">
                                            @foreach ($main_banner as $key => $banner)
                                                <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                                                    <a href="{{ $banner['url'] }}">
                                                        <img class="d-block w-100" style="max-height: 550px"
                                                            onerror="this.src='{{ asset('public/assets/front-end/img/image-place-holder.png') }}'"
                                                            src="{{ asset('storage/app/public/banner') }}/{{ $banner['photo'] }}"
                                                            alt="">
                                                    </a>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End of Banner-section -->


            <div class="container" style="padding-top: 40px;">

                <h2 class="title title-underline mb-4 appear-animate">Top Vendors</h2>
                <div class="owl-carousel owl-theme row cols-lg-4 cols-md-3 cols-sm-2 cols-1 mb-10 pb-2 appear-animate"
                    style="padding: 20px;" data-owl-options="{
                                                        'nav': false,
                                                        'dots': true,
                                                        'margin': 20,
                                                        'responsive': {
                                                            '0': {
                                                                'items': 1
                                                            },
                                                            '576': {
                                                                'items': 2
                                                            },
                                                            '768': {
                                                                'items': 3
                                                            },
                                                            '1200': {
                                                                'items': 4
                                                            }
                                                        }
                                                    }">
                    @php($flash_deals = \App\Model\FlashDeal::with(['products.product.reviews'])->where(['status' => 1])->where(['deal_type' => 'flash_deal'])->whereDate('start_date', '<=', date('Y-m-d'))->whereDate('end_date', '>=', date('Y-m-d'))->first())

                    @if (isset($flash_deals))
                        <div class="vendor-widget mb-0">
                            <div class="vendor-widget-2">
                                <div class="vendor-details">
                                    <figure class="vendor-logo">
                                        <a href="#">
                                            <img src="assets/images/demos/demo9/vendor-logo/4.jpg" alt="Vendor Logo"
                                                width="70" height="70" />
                                        </a>
                                    </figure>
                                    <div class="vendor-personal">
                                        <h4 class="vendor-name">
                                            <a href="#">{{ $flash_deals['title'] }}</a>
                                        </h4>
                                        <span class="vendor-product-count">(27 Products)</span>
                                        <div class="ratings-container">
                                            <div class="ratings-full">
                                                <span class="ratings" style="width: 100%;"></span>
                                                <span class="tooltiptext tooltip-top"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="vendor-products row cols-3 gutter-sm">
                                    <div class="vendor-product">
                                        <figure class="product-media">
                                            <a href="#">
                                                <img src="assets/images/watch.jpg" alt="Vendor Product" width="100"
                                                    height="113" />
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="vendor-product">
                                        <figure class="product-media">
                                            <a href="#">
                                                <img src="assets/images/watch.jpg" alt="Vendor Product" width="100"
                                                    height="113" />
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="vendor-product">
                                        <figure class="product-media">
                                            <a href="#">
                                                <img src="assets/images/watch.jpg" alt="Vendor Product" width="100"
                                                    height="113" />
                                            </a>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>

                    @endif
                    <!-- End of Vendor widget 2 -->
                    <div class="vendor-widget mb-0">
                        <div class="vendor-widget-2">
                            <div class="vendor-details">
                                <figure class="vendor-logo">
                                    <a href="#">
                                        <img src="assets/images/demos/demo9/vendor-logo/4.jpg" alt="Vendor Logo" width="70"
                                            height="70" />
                                    </a>
                                </figure>
                                <div class="vendor-personal">
                                    <h4 class="vendor-name">
                                        <a href="#">Apple</a>
                                    </h4>
                                    <span class="vendor-product-count">(20 Products)</span>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width: 100%;"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vendor-products row cols-3 gutter-sm">
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="#">
                                            <img src="assets/images/demos/demo9/product/4-4.jpg" alt="Vendor Product"
                                                width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="#">
                                            <img src="assets/images/demos/demo9/product/4-5.jpg" alt="Vendor Product"
                                                width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="#">
                                            <img src="assets/images/demos/demo9/product/4-6.jpg" alt="Vendor Product"
                                                width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Vendor widget 2 -->
                    <div class="vendor-widget mb-0">
                        <div class="vendor-widget-2">
                            <div class="vendor-details">
                                <figure class="vendor-logo">
                                    <a href="#">
                                        <img src="assets/images/demos/demo9/vendor-logo/4.jpg" alt="Vendor Logo" width="70"
                                            height="70" />
                                    </a>
                                </figure>
                                <div class="vendor-personal">
                                    <h4 class="vendor-name">
                                        <a href="#">Apple</a>
                                    </h4>
                                    <span class="vendor-product-count">(30 Products)</span>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width: 100%;"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vendor-products row cols-3 gutter-sm">
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-7.jpg" alt="Vendor Product"
                                                width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-8.jpg" alt="Vendor Product"
                                                width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-9.jpg" alt="Vendor Product"
                                                width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Vendor widget 2 -->
                    <div class="vendor-widget mb-0">
                        <div class="vendor-widget-2">
                            <div class="vendor-details">
                                <figure class="vendor-logo">
                                    <a href="vendor-dokan-store.html">
                                        <img src="assets/images/demos/demo9/vendor-logo/4.jpg" alt="Vendor Logo" width="70"
                                            height="70" />
                                    </a>
                                </figure>
                                <div class="vendor-personal">
                                    <h4 class="vendor-name">
                                        <a href="vendor-dokan-store.html">Apple</a>
                                    </h4>
                                    <span class="vendor-product-count">(17 Products)</span>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width: 100%;"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vendor-products row cols-3 gutter-sm">
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-10.jpg" alt="Vendor Product"
                                                width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-11.jpg" alt="Vendor Product"
                                                width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-12.jpg" alt="Vendor Product"
                                                width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Vendor widget 2 -->
                    <!-- End of Vendor widget 2 -->
                    <div class="vendor-widget mb-0">
                        <div class="vendor-widget-2">
                            <div class="vendor-details">
                                <figure class="vendor-logo">
                                    <a href="vendor-dokan-store.html">
                                        <img src="assets/images/demos/demo9/vendor-logo/4.jpg" alt="Vendor Logo" width="70"
                                            height="70" />
                                    </a>
                                </figure>
                                <div class="vendor-personal">
                                    <h4 class="vendor-name">
                                        <a href="vendor-dokan-store.html">Apple</a>
                                    </h4>
                                    <span class="vendor-product-count">(17 Products)</span>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width: 100%;"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vendor-products row cols-3 gutter-sm">
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-10.jpg" alt="Vendor Product"
                                                width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-11.jpg" alt="Vendor Product"
                                                width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-12.jpg" alt="Vendor Product"
                                                width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Vendor widget 2 -->
                    <!-- End of Vendor widget 2 -->
                    <div class="vendor-widget mb-0">
                        <div class="vendor-widget-2">
                            <div class="vendor-details">
                                <figure class="vendor-logo">
                                    <a href="vendor-dokan-store.html">
                                        <img src="assets/images/demos/demo9/vendor-logo/4.jpg" alt="Vendor Logo" width="70"
                                            height="70" />
                                    </a>
                                </figure>
                                <div class="vendor-personal">
                                    <h4 class="vendor-name">
                                        <a href="vendor-dokan-store.html">Apple</a>
                                    </h4>
                                    <span class="vendor-product-count">(17 Products)</span>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width: 100%;"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vendor-products row cols-3 gutter-sm">
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-10.jpg" alt="Vendor Product"
                                                width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-11.jpg" alt="Vendor Product"
                                                width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-12.jpg" alt="Vendor Product"
                                                width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Vendor widget 2 -->
                    <!-- End of Vendor widget 2 -->
                    <div class="vendor-widget mb-0">
                        <div class="vendor-widget-2">
                            <div class="vendor-details">
                                <figure class="vendor-logo">
                                    <a href="vendor-dokan-store.html">
                                        <img src="assets/images/demos/demo9/vendor-logo/4.jpg" alt="Vendor Logo" width="70"
                                            height="70" />
                                    </a>
                                </figure>
                                <div class="vendor-personal">
                                    <h4 class="vendor-name">
                                        <a href="vendor-dokan-store.html">Apple</a>
                                    </h4>
                                    <span class="vendor-product-count">(17 Products)</span>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width: 100%;"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vendor-products row cols-3 gutter-sm">
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-10.jpg" alt="Vendor Product"
                                                width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-11.jpg" alt="Vendor Product"
                                                width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-12.jpg" alt="Vendor Product"
                                                width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Vendor widget 2 -->

                </div>
                <!-- End of Owl Carousel -->


                <!--Season Sale-->

                <div class="category-banner-wrapper row grid appear-animate fadeIn appear-animation-visible"
                    style="position: relative; height: 480px; animation-duration: 1.2s;">
                    <div class="grid-item col-lg-6 col-md-8 col-sm-7 height-x2"
                        style="position: absolute; left: 0%; top: 0px;">
                        <div class="banner banner-fixed br-sm banner-lg">
                            <figure>
                                <img src="assets/images/1-1.jpg" alt="Banner" width="640" height="460"
                                    style="background-color: #F6F6F6;">
                            </figure>
                            <div class="banner-content y-50">
                                <h3 class="banner-title text-capitalize ls-25 mb-0">Season<br>Women Fashion
                                    <br>Collection
                                </h3>
                                <p>Free shipping on all over <strong class="text-secondary">$99</strong></p>
                                <a href="shop-banner-sidebar.html"
                                    class="btn btn-dark btn-outline btn-rounded btn-icon-right">
                                    Shop Now<i class="w-icon-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item col-lg-3 col-sm-6 height-x1"
                        style="position: absolute; left: 49.9948%; top: 0px;">
                        <div class="banner banner-fixed br-sm banner-sm">
                            <figure>
                                <img src="assets/images/1-2.jpg" alt="Banner" width="310" height="220"
                                    style="background-color: #F2F2F2;">
                            </figure>
                            <div class="banner-content y-50 pt-2">
                                <h5 class="banner-subtitle font-weight-bold text-capitalize mb-2 ls-25">Best Seller</h5>
                                <h3 class="banner-title text-uppercase mb-0">Cosmetic</h3>
                                <div class="banner-price-info text-secondary text-capitalize ls-25">45% Off</div>
                                <a href="shop-banner-sidebar.html"
                                    class="btn btn-dark btn-link btn-underline btn-icon-right">
                                    Shop Now<i class="w-icon-long-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item col-lg-3 col-md-4 col-sm-5 height-x2"
                        style="position: absolute; left: 74.9922%; top: 0px;">
                        <div class="banner banner-fixed br-sm banner-md">
                            <figure>
                                <img src="assets/images/1-3.jpg" alt="Banner" width="310" height="220"
                                    style="background-color: #3D4753;">
                            </figure>
                            <div class="banner-content x-50 w-100 text-center">
                                <div class="banner-price-info text-white font-weight-bolder text-uppercase ls-25">40
                                    <sup class="font-weight-bold p-relative">%</sup>
                                    <sub class="font-weight-bold p-relative ls-normal">Off</sub>
                                </div>
                                <h3 class="banner-title text-uppercase text-white">Ultimate Sale</h3>
                                <p class="text-capitalize">Discount Selected Items</p>
                                <a href="shop-banner-sidebar.html"
                                    class="btn btn-white btn-outline btn-rounded btn-icon-right">
                                    Shop Now<i class="w-icon-long-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item col-lg-3 col-sm-6 height-x1"
                        style="position: absolute; left: 49.9948%; top: 240px;">
                        <div class="banner banner-fixed br-sm banner-sm">
                            <figure>
                                <img src="assets/images/1-4.jpg" alt="Banner" width="310" height="220"
                                    style="background-color: #414A59;">
                            </figure>
                            <div class="banner-content y-50 pt-2">
                                <h5 class="banner-subtitle text-white font-weight-bold text-capitalize mb-2 ls-25">
                                    Featured Event</h5>
                                <h3 class="banner-title text-white text-uppercase mb-0 ls-25">Electronics</h3>
                                <div class="banner-price-info text-primary text-capitalize ls-25">Sale</div>
                                <a href="shop-banner-sidebar.html"
                                    class="btn btn-white btn-link btn-underline btn-icon-right">
                                    Shop Now<i class="w-icon-long-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="grid-space col-1" style="position: absolute; left: 0%; top: 480px;"></div>
                </div>







                <!-- gdrgdddddddddddddddd -->

                <div class="row grid grid-float pt-2 banner-grid mb-9 appear-animate">
                    <div class="grid-item col-lg-6 height-x2">
                        <div class="banner banner-fixed banner-lg br-sm">
                            <figure>
                                <img src="assets/images/demos/demo9/banner/1-1.jpg" alt="Banner" width="670" height="450"
                                    style="background-color: #E3E7EA;" />
                            </figure>
                            <div class="banner-content y-50">
                                <h5 class="banner-subtitle text-capitalize font-weight-normal mb-0 ls-25">
                                    Flash Sale <strong class="text-secondary text-uppercase">50% Off</strong>
                                </h5>
                                <h3 class="banner-title text-capitalize">Kitchen Collection</h3>
                                <p>Only until the end of this Week</p>
                                <a href="demo9-shop.html" class="btn btn-dark btn-outline btn-rounded btn-icon-right">
                                    Shop Now<i class="w-icon-long-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item col-lg-6 height-x1">
                        <div class="banner banner-fixed banner-md br-sm">
                            <figure>
                                <img src="assets/images/demos/demo9/banner/1-2.jpg" alt="Banner" width="670" height="450"
                                    style="background-color: #2D2E32;" />
                            </figure>
                            <div class="banner-content">
                                <h3 class="banner-title text-white ls-25">
                                    Accessories<br><span class="font-weight-normal ls-normal">Collection</span>
                                </h3>
                                <a href="demo9-shop.html" class="btn btn-white btn-link btn-underline btn-icon-right">
                                    Shop Now<i class="w-icon-long-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item col-sm-6 col-lg-3 height-x1">
                        <div class="banner banner-fixed banner-sm br-sm">
                            <figure>
                                <img src="assets/images/demos/demo9/banner/1-3.jpg" alt="Banner" width="330" height="215"
                                    style="background-color: #181818;" />
                            </figure>
                            <div class="banner-content x-50 y-50 w-100 text-center">
                                <h3 class="banner-title font-secondary font-weight-normal mb-0 ls-25">Sale</h3>
                                <div class="banner-price-info font-weight-normal text-white mb-3">
                                    Up to <strong class="text-uppercase">20% Off</strong>
                                </div>
                                <a href="demo9-shop.html" class="btn btn-white btn-link btn-underline">Shop
                                    Collection</a>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item col-sm-6 col-lg-3 height-x1">
                        <div class="banner banner-fixed banner-sm br-sm">
                            <figure>
                                <img src="assets/images/demos/demo9/banner/1-4.jpg" alt="Banner" width="330" height="215"
                                    style="background-color: #A3A8A6;" />
                            </figure>
                            <div class="banner-content">
                                <h5 class="banner-subtitle text-uppercase font-weight-bold">20% Off</h5>
                                <h3 class="banner-title text-capitalize ls-25">Kids Store</h3>
                                <a href="" class="btn btn-dark btn-link btn-underline btn-icon-right">
                                    Shop Now<i class="w-icon-long-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Banner Grid -->
                <!-- End of Banner Grid -->

                <!-- Consumer Electronics-->
                <div class="filter-with-title appear-animate">
                    <h2 class="title">Consumer Electronics</h2>

                </div>
                <div class="row grid cols-xl-5 cols-lg-4 cols-sm-3 cols-2 appear-animate" id="products-2">
                    @foreach ($latest_products as $product)
                        <div class="grid-item 1-1 1-3">
                            @include('web-views.partials._single-product',['product'=>$product])

                        </div>
                    @endforeach

                    <!-- End of Grid Item -->
                    <div class="grid-space col-xl-5col col-1"></div>
                </div>

                <!---->
                <div class="filter-with-title appear-animate">
                    <h2 class="title">Consumer Electronics</h2>
                </div>
                <div class="row grid cols-xl-5 cols-lg-4 cols-sm-3 cols-2 appear-animate" id="products-2">
                    @foreach ($latest_products as $product)
                        <div class="grid-item 1-1 1-3">
                            @include('web-views.partials._single-product',['product'=>$product])
                        </div>
                    @endforeach
                    <div class="grid-space col-xl-5col col-1"></div>
                </div>







                <div class="product-wrapper-1 appear-animate mb-5 fadeIn appear-animation-visible"
                    style="animation-duration: 1.2s;">
                    <div class="title-link-wrapper pb-1 mb-4">
                        <h2 class="title ls-normal mb-0">Clothing &amp; Apparel</h2>
                        <a href="shop-boxed-banner.html" class="font-size-normal font-weight-bold ls-25 mb-0">More
                            Products<i class="w-icon-long-arrow-right"></i></a>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-sm-4 mb-4">
                            <div class="banner h-100 br-sm" style="background-color: #808080;">

                                <div class="banner-content content-top">
                                    <h5 class="banner-subtitle font-weight-normal mb-2">
                                        {{ trans('messages.deal_of_the_day') }}</h5>
                                    <hr class="banner-divider bg-dark mb-2">
                                    <h3 class="banner-title font-weight-bolder ls-25 text-uppercase">
                                        New Arrivals<br> <span class="font-weight-normal text-capitalize"><strong
                                                style="font-size: 21px!important;color: {{ $web_config['primary_color'] }}">
                                                {{ $deal_of_the_day->discount_type == 'amount' ? \App\CPU\Helpers::currency_converter($deal_of_the_day->discount) : $deal_of_the_day->discount . ' % ' }}
                                                OFF
                                            </strong></span>
                                    </h3>
                                    <div class="d-flex justify-content-center align-items-center"
                                        style="padding-top: 37px">
                                        <img style="height: 206px;"
                                            src="{{ \App\CPU\ProductManager::product_image_path('thumbnail') }}/{{ $deal_of_the_day->product['thumbnail'] }}"
                                            onerror="this.src='{{ asset('public/assets/front-end/img/image-place-holder.png') }}'"
                                            alt="">
                                    </div>
                                    <div style="text-align: center; padding-top: 26px;">
                                        <h5 style="font-weight: 600; color: {{ $web_config['primary_color'] }}">
                                            {{ substr($deal_of_the_day->product['name'], 0, 40) }}
                                            {{ strlen($deal_of_the_day->product['name']) > 40 ? '...' : '' }}
                                        </h5>
                                        <span class="text-accent">
                                            {{ \App\CPU\Helpers::currency_converter($deal_of_the_day->product->unit_price - \App\CPU\Helpers::get_product_discount($deal_of_the_day->product, $deal_of_the_day->product->unit_price)) }}
                                        </span>
                                        @if ($deal_of_the_day->product->discount > 0)
                                            <strike style="font-size: 12px!important;color: grey!important;">
                                                {{ \App\CPU\Helpers::currency_converter($deal_of_the_day->product->unit_price) }}
                                            </strike>
                                        @endif

                                    </div>
                                    <div class="pt-3 pb-2" style="text-align: center;">
                                        {{-- <button class="buy_btn" --}}
                                        {{-- onclick="location.href='{{route('product',$deal_of_the_day->product->slug)}}'">{{trans('messages.buy_now')}} --}}
                                        <a href='{{ route('product', $deal_of_the_day->product->slug) }}'
                                            class="btn btn-dark btn-outline btn-rounded btn-sm">shop Now</a>
                                        {{-- </button> --}}
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- End of Banner -->
                        <div class="col-lg-9 col-sm-8">
                            <div class="owl-carousel owl-theme owl-loaded owl-drag" data-owl-options="{
                                        'nav': false,
                                        'dots': true,
                                        'margin': 20,
                                        'responsive': {
                                            '0': {
                                                'items': 2
                                            },
                                            '576': {
                                                'items': 2
                                            },
                                            '992': {
                                                'items': 3
                                            },
                                            '1200': {
                                                'items': 4
                                            }
                                        }
                                    }">




                                <div class="owl-stage-outer">
                                    <div class="owl-stage"
                                        style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 945px;">
                                        @foreach ($latest_products as $product)
                                            <div class="owl-item active" style="width: 216.25px; margin-right: 20px;">
                                                {{-- <div class="product-col">
                                                <div class="product-wrap product text-center">
                                                    
                                                </div>
                                                
                                            </div> --}}
                                                @include('web-views.partials._single-product',['product'=>$product])
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="owl-nav disabled"><button type="button" role="presentation"
                                        class="owl-prev"><i class="w-icon-angle-left"></i></button><button
                                        type="button" role="presentation" class="owl-next"><i
                                            class="w-icon-angle-right"></i></button></div>
                                <div class="owl-dots disabled"><button role="presentation"
                                        class="owl-dot active"><span></span></button></div>
                            </div>
                        </div>
                    </div>
                </div>


                <!--Consumer Electronics-->
                <div class="filter-with-title appear-animate">
                    <h2 class="title">Consumer Electronics</h2>

                </div>
                <div class="row grid cols-xl-5 cols-lg-4 cols-sm-3 cols-2 appear-animate" id="products-2">
                    @foreach ($latest_products as $product)
                        <div class="grid-item 1-1 1-3">
                            @include('web-views.partials._single-product',['product'=>$product])

                        </div>
                    @endforeach

                    <!-- End of Grid Item -->

                    <!-- End of Grid Item -->
                    <div class="grid-space col-xl-5col col-1"></div>
                </div>





                <div class="sale-banner banner br-sm appear-animate">
                    <div class="banner-content">
                        <h4
                            class="content-left banner-subtitle text-uppercase mb-8 mb-md-0 mr-0 mr-md-4 text-secondary ls-25">
                            <span class="text-dark font-weight-bold lh-1 ls-normal">Up
                                <br>To</span>20% Sale!
                        </h4>
                        <div class="content-right">
                            <h3 class="banner-title text-uppercase font-weight-normal mb-4 mb-md-0 ls-25 text-white">
                                <span>Pay Only For
                                    <strong class="mr-10 pr-lg-10">Your Lovling Electronics</strong>
                                    Pay Only For
                                    <strong class="mr-10 pr-lg-10">Your Lovling Electronics</strong>
                                    Pay Only For
                                    <strong class="mr-10 pr-lg-10">Your Lovling Electronics</strong>
                                </span>
                            </h3>
                            <a href="demo9-shop.html" class="btn btn-white btn-rounded">Shop Now
                                <i class="w-icon-long-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>





                <div class="filter-with-title appear-animate">
                    <h2 class="title">Consumer Electronics</h2>

                </div>
                <div class="row grid cols-xl-5 cols-lg-4 cols-sm-3 cols-2 appear-animate" id="products-2">
                    @foreach ($latest_products as $product)
                        <div class="grid-item 1-1 1-3">
                            @include('web-views.partials._single-product',['product'=>$product])
                            {{-- <div class="product-wrap product text-center">
                    @include('web-views.partials._single-product',['product'=>$product]) --}}
                        </div>
                    @endforeach

                    <!-- End of Grid Item -->

                    <!-- End of Grid Item -->
                    <div class="grid-space col-xl-5col col-1"></div>
                </div>



                <div class="row cols-md-2 mb-5 appear-animate">
                    <div class="banner banner-fixed category-banner mb-5">
                        <figure class="br-sm">
                            <img src="assets/images/demos/demo9/banner/2-1.jpg" alt="Category Banner" width="640"
                                height="200" style="background-color: #32373B;" />
                        </figure>
                        <div class="banner-content y-50">
                            <h5 class="banner-subtitle text-uppercase text-secondary font-weight-bold">New Arrivals</h5>
                            <h3 class="banner-title text-white text-capitalize font-weight-normal mb-5 ls-25">
                                <strong>Flash Wireless</strong><br>Earphones
                            </h3>
                            <a href="demo9-shop.html" class="btn btn-white btn-link btn-underline btn-icon-right">
                                Shop Now<i class="w-icon-long-arrow-right"></i>
                            </a>
                        </div>
                    </div>

                    <!-- End of Banner -->
                    <div class="banner banner-fixed category-banner mb-5">
                        <figure class="br-sm">
                            <img src="assets/images/demos/demo9/banner/2-2.jpg" alt="Category Banner" width="640"
                                height="200" style="background-color: #ECECEE;" />
                        </figure>
                        <div class="banner-content y-50">
                            <h5 class="banner-subtitle text-capitalize font-weight-normal ls-25">
                                Flash Sale <span class="text-secondary text-uppercase">50% Off</span>
                            </h5>
                            <h3 class="banner-title text-capitalize font-weight-normal mb-5 ls-25">
                                <strong>Fashion Figure</strong><br>Skate Sale
                            </h3>
                            <a href="demo9-shop.html" class="btn btn-dark btn-link btn-underline btn-icon-right">
                                Shop Now<i class="w-icon-long-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                    <!-- End of Banner -->
                </div>


                <!-- End of Row -->




                <div class="filter-with-title appear-animate">
                    <h2 class="title">Consumer Electronics</h2>
                    <ul class="nav-filters filter-boxed" data-target="#products-2">

                    </ul>
                </div>
                <div class="row grid cols-xl-5 cols-lg-4 cols-sm-3 cols-2 appear-animate" id="products-2">
                    @foreach ($latest_products as $product)
                        <div class="grid-item 1-1 1-3">
                            @include('web-views.partials._single-product',['product'=>$product])
                            {{-- <div class="product-wrap product text-center">
                    @include('web-views.partials._single-product',['product'=>$product]) --}}
                        </div>
                    @endforeach

                    <!-- End of Grid Item -->

                    <!-- End of Grid Item -->
                    <div class="grid-space col-xl-5col col-1"></div>
                </div>


                <div class="banner link-banner-newsletter br-sm appear-animate" style="background-image: url(assets/images/demos/demo9/banner/3.jpg);
                                                        background-color: #27393D;">
                    <div class="content-left mr-auto">
                        <h3 class="banner-title text-white text-capitalize font-weight-bold ls-25">Download kartcult App
                            Now!
                        </h3>
                        <p class="text-white">Shopping fastly and easily more with our app. Get a link to download the
                            app
                            on your phone.</p>
                        <form action="#" method="get" class="input-wrapper input-wrapper-inline input-wrapper-rounded">
                            <input class="form-control mb-4" type="email" placeholder="Enter Your Email..." name="email"
                                id="email_4">
                            <button class="btn btn-primary btn-rounded mb-4" type="submit">Send Link
                            </button>
                        </form>
                    </div>
                    <!-- End of Content Left -->
                    <div class="content-right">
                        <a href="#">
                            <img src="assets/images/demos/demo9/banner/button-1.jpg" alt="Button" width="141" height="41"
                                style="background-color: #121315" />
                        </a>
                        <a href="#">
                            <img src="assets/images/demos/demo9/banner/button-2.jpg" alt="Button" width="141" height="41"
                                style="background-color: #121315" />
                        </a>
                    </div>
                    <!-- End of Content Right -->
                </div>
                <!-- End of Link Banner Newsletter -->
            </div>
        </main>
    </div>
@endsection

@push('script')
    <!-- Plugin JS File -->
    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/parallax/parallax.min.js"></script>
    <script src="assets/vendor/jquery.plugin/jquery.plugin.min.js"></script>
    <script src="assets/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
    <script src="assets/vendor/isotope/isotope.pkgd.min.js"></script>
    <script src="assets/vendor/owl-carousel/owl.carousel.min.js"></script>
    <script src="assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="assets/vendor/skrollr/skrollr.min.js"></script>
    <script src="assets/vendor/jquery.countdown/jquery.countdown.min.js"></script>
    <script src="assets/vendor/isotope/isotope.pkgd.min.js"></script>

    <!-- Main JS -->
    <script src="assets/js/main.min.js"></script>


@endpush
