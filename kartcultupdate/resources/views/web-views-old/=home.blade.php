@extends('layouts.front-end.app')

@section('title','Welcome To '. $web_config['name']->value.' Home')

@push('css_or_js')
    <meta property="og:image" content="{{asset('storage/app/public/company')}}/{{$web_config['web_logo']->value}}"/>
    <meta property="og:title" content="Welcome To {{$web_config['name']->value}} Home"/>
    <meta property="og:url" content="{{env('APP_URL')}}">
    <meta property="og:description" content="{!! substr($web_config['about']->value,0,100) !!}">

    <meta property="twitter:card" content="{{asset('storage/app/public/company')}}/{{$web_config['web_logo']->value}}"/>
    <meta property="twitter:title" content="Welcome To {{$web_config['name']->value}} Home"/>
    <meta property="twitter:url" content="{{env('APP_URL')}}">
    <meta property="twitter:description" content="{!! substr($web_config['about']->value,0,100) !!}">

    <link rel="stylesheet" href="{{asset('public/assets/front-end')}}/css/home.css"/>
    <style>
        .media {
            background: white;
        }

        .cz-countdown-days {
            color: white !important;
            background-color: {{$web_config['primary_color']}};
            padding: 0px 6px;
            border-radius: 3px;
            margin-right: 3px !important;
        }

        .cz-countdown-hours {
            color: white !important;
            background-color: {{$web_config['primary_color']}};
            padding: 0px 6px;
            border-radius: 3px;
            margin-right: 3px !important;
        }

        .cz-countdown-minutes {
            color: white !important;
            background-color: {{$web_config['primary_color']}};
            padding: 0px 6px;
            border-radius: 3px;
            margin-right: 3px !important;
        }

        .cz-countdown-seconds {
            color: {{$web_config['primary_color']}};
            border: 1px solid{{$web_config['primary_color']}};
            padding: 0px 6px;
            border-radius: 3px !important;
        }

        .flash_deal_product_details .flash-product-price {
            font-weight: 700;
            font-size: 18px;
            color: {{$web_config['primary_color']}};
        }

        .featured_deal_left {
            height: 130px;
            background: {{$web_config['primary_color']}} 0% 0% no-repeat padding-box;
            padding: 10px 100px;
            text-align: center;
        }

        .featured_deal {
            min-height: 130px;

        }

        .category_div:hover {
            color: {{$web_config['secondary_color']}};
        }

        .deal_of_the_day {
            /* filter: grayscale(0.5); */
            opacity: .8;
            background: {{$web_config['secondary_color']}};
            border-radius: 3px;
        }

        .deal-title {
            font-size: 12px;

        }

        .for-flash-deal-img img {
            max-width: none;
        }

        @media (max-width: 375px) {
            .cz-countdown {
                display: flex !important;

            }

            .cz-countdown .cz-countdown-seconds {

                margin-top: -5px !important;
            }

            .for-feature-title {
                font-size: 20px !important;
            }
        }

        @media (max-width: 600px) {
            .flash_deal_title {
                font-weight: 600;
                font-size: 18px;
                text-transform: uppercase;
            }

            .cz-countdown .cz-countdown-value {
                font-family: "Roboto", sans-serif;
                font-size: 11px !important;
                font-weight: 700 !important;
            }

            .featured_deal {
                opacity: 1 !important;
            }

            .cz-countdown {
                display: inline-block;
                flex-wrap: wrap;
                font-weight: normal;
                margin-top: 4px;
                font-size: smaller;
            }

            .view-btn-div-f {

                margin-top: 6px;
                float: right;
            }

            .view-btn-div {
                float: right;
            }

            .viw-btn-a {
                font-size: 10px;
                font-weight: 600;
            }


            .for-mobile {
                display: none;
            }

            .featured_for_mobile {
                max-width: 100%;
                margin-top: 20px;
                margin-bottom: 20px;
            }
        }

        @media (max-width: 360px) {
            .featured_for_mobile {
                max-width: 100%;
                margin-top: 10px;
                margin-bottom: 10px;
            }

            .featured_deal {
                opacity: 1 !important;
            }
        }

        @media (max-width: 375px) {
            .featured_for_mobile {
                max-width: 100%;
                margin-top: 10px;
                margin-bottom: 10px;
            }

            .featured_deal {
                opacity: 1 !important;
            }
        }

        @media (min-width: 768px) {
            .displayTab {
                display: block !important;
            }
        }

        @media (max-width: 800px) {
            .for-tab-view-img {
                width: 40%;
            }

            .for-tab-view-img {
                width: 105px;
            }

            .widget-title {
                font-size: 19px !important;
            }
        }

        .featured_deal_carosel .carousel-inner {
            width: 100% !important;
        }

        .badge-style2 {
            color: black !important;
            background: transparent !important;
            font-size: 11px;
        }
    </style>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"
          integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css"
          integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>

@endpush

@section('content')

<div class="category-wrapper owl-carousel owl-theme appear-animate owl-loaded owl-drag fadeIn appear-animation-visible" style="padding: 12px;animation-duration: 1.2s;background: #f0f0f0;" data-owl-options="{ 'nav': false,'dots': false,'margin': 34, 'responsive': { '0': {'items': 4},'576': { 'items': 5},'768': { 'items': 6},'992': {'items': 8 },'1200': {  'items': 12 } }
}">

    <div class="owl-stage-outer">
        <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0.7s ease 0s; width: 1920px;">
            <div class="owl-item active" style="width: 86.6px; margin-right: 40px;">
                <div class="category category-ellipse">
                    <figure class="category-media">
                    <a href="electronicslist.html">
                            <img src="{{asset('public/assets/front-end/cartKult/assets/images/list-1.jpg')}}" alt="Categroy" width="190" height="190" style="background-color: #9BC4CA;">
                      </a>
                    </figure>
                    <div class="category-content">
                        <h4 class="category-name">
                            <a href="electronicslist.html">Laptops</a>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="owl-item active" style="width: 86.6px; margin-right: 40px;">
                <div class="category category-ellipse">
                    <figure class="category-media">
                        <a href="mobile_gadgetslist.html">
                            <img src="{{asset('public/assets/front-end/cartKult/assets/images/list-2.jpg')}}" alt="Categroy" width="190" height="190" style="background-color: #9BC4CA;">
                        </a>
                    </figure>
                    <div class="category-content">
                        <h4 class="category-name">
                            <a href="mobile_gadgetslist.html">ACCESSORIES</a>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="owl-item active" style="width: 86.6px; margin-right: 40px;">
                <div class="category category-ellipse">
                    <figure class="category-media">
                        <a href="furniturelist.html">
                            <img src="{{asset('public/assets/front-end/cartKult/assets/images/list-3.jpg')}}" alt="Categroy" width="190" height="190" style="background-color: #9BC4CA;">
                        </a>
                    </figure>
                    <div class="category-content">
                        <h4 class="category-name">
                            <a href="furniturelist.html">Furniture</a>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="owl-item active" style="width: 86.6px; margin-right: 40px;">
                <div class="category category-ellipse">
                    <figure class="category-media">
                        <a href="electronicslist.html">
                            <img src="{{asset('public/assets/front-end/cartKult/assets/images/list-4.jpg')}}" alt="Categroy" width="190" height="190" style="background-color: #9BC4CA;">
                        </a>
                    </figure>
                    <div class="category-content">
                        <h4 class="category-name">
                            <a href="electronicslist.html">Electrolier</a>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="owl-item active" style="width: 86.6px; margin-right: 40px;">
                <div class="category category-ellipse">
                    <figure class="category-media">
                        <a href="electronicslist.html">
                            <img src="{{asset('public/assets/front-end/cartKult/assets/images/list-1.jpg')}}" alt="Categroy" width="190" height="190" style="background-color: #9BC4CA;">
                        </a>
                    </figure>
                    <div class="category-content">
                        <h4 class="category-name">
                            <a href="electronicslist.html">Laptops</a>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="owl-item active" style="width: 86.6px; margin-right: 40px;">
                <div class="category category-ellipse">
                    <figure class="category-media">
                        <a href="mobile_gadgetslist.html">
                            <img src="{{asset('public/assets/front-end/cartKult/assets/images/list-2.jpg')}}" alt="Categroy" width="190" height="190" style="background-color: #9BC4CA;">
                        </a>
                    </figure>
                    <div class="category-content">
                        <h4 class="category-name">
                            <a href="mobile_gadgetslist.html">ACCESSORIES</a>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="owl-item active" style="width: 86.6px; margin-right: 40px;">
                <div class="category category-ellipse">
                    <figure class="category-media">
                        <a href="furniturelist.html">
                            <img src="{{asset('public/assets/front-end/cartKult/assets/images/list-3.jpg')}}" alt="Categroy" width="190" height="190" style="background-color: #9BC4CA;">
                        </a>
                    </figure>
                    <div class="category-content">
                        <h4 class="category-name">
                            <a href="furniturelist.html">Furniture</a>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="owl-item active" style="width: 86.6px; margin-right: 40px;">
                <div class="category category-ellipse">
                    <figure class="category-media">
                        <a href="electronicslist.html">
                            <img src="{{asset('public/assets/front-end/cartKult/assets/images/list-4.jpg')}}" alt="Categroy" width="190" height="190" style="background-color: #9BC4CA;">
                        </a>
                    </figure>
                    <div class="category-content">
                        <h4 class="category-name">
                            <a href="electronicslist.html">Electrolier</a>
                        </h4>
                    </div>
                </div>
            </div>

            <div class="owl-item active" style="width: 86.6px; margin-right: 40px;">
                <div class="category category-ellipse">
                    <figure class="category-media">
                        <a href="electronicslist.html">
                            <img src="{{asset('public/assets/front-end/cartKult/assets/images/list-1.jpg')}}" alt="Categroy" width="190" height="190" style="background-color: #9BC4CA;">
                        </a>
                    </figure>
                    <div class="category-content">
                        <h4 class="category-name">
                            <a href="electronicslist.html">Laptops</a>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="owl-item active" style="width: 86.6px; margin-right: 40px;">
                <div class="category category-ellipse">
                    <figure class="category-media">
                        <a href="mobile_gadgetslist.html">
                            <img src="{{asset('public/assets/front-end/cartKult/assets/images/list-2.jpg')}}" alt="Categroy" width="190" height="190" style="background-color: #9BC4CA;">
                        </a>
                    </figure>
                    <div class="category-content">
                        <h4 class="category-name">
                            <a href="mobile_gadgetslist.html">ACCESSORIES</a>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="owl-item active" style="width: 86.6px; margin-right: 40px;">
                <div class="category category-ellipse">
                    <figure class="category-media">
                        <a href="furniturelist.html">
                            <img src="{{asset('public/assets/front-end/cartKult/assets/images/list-3.jpg')}}" alt="Categroy" width="190" height="190" style="background-color: #9BC4CA;">
                        </a>
                    </figure>
                    <div class="category-content">
                        <h4 class="category-name">
                            <a href="furniturelist.html">Furniture</a>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="owl-item active" style="width: 86.6px; margin-right: 40px;">
                <div class="category category-ellipse">
                    <figure class="category-media">
                        <a href="electronicslist.html">
                            <img src="{{asset('public/assets/front-end/cartKult/assets/images/list-4.jpg')}}" alt="Categroy" width="190" height="190" style="background-color: #9BC4CA;">
                        </a>
                    </figure>
                    <div class="category-content">
                        <h4 class="category-name">
                            <a href="electronicslist.html">Electrolier</a>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="owl-item active" style="width: 86.6px; margin-right: 40px;">
                <div class="category category-ellipse">
                    <figure class="category-media">
                        <a href="electronicslist.html">
                            <img src="{{asset('public/assets/front-end/cartKult/assets/images/list-1.jpg')}}" alt="Categroy" width="190" height="190" style="background-color: #9BC4CA;">
                        </a>
                    </figure>
                    <div class="category-content">
                        <h4 class="category-name">
                            <a href="electronicslist.html">Laptops</a>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="owl-item active" style="width: 86.6px; margin-right: 40px;">
                <div class="category category-ellipse">
                    <figure class="category-media">
                        <a href="mobile_gadgetslist.html">
                            <img src="{{asset('public/assets/front-end/cartKult/assets/images/list-2.jpg')}}" alt="Categroy" width="190" height="190" style="background-color: #9BC4CA;">
                        </a>
                    </figure>
                    <div class="category-content">
                        <h4 class="category-name">
                            <a href="mobile_gadgetslist.html">ACCESSORIES</a>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="owl-item active" style="width: 86.6px; margin-right: 40px;">
                <div class="category category-ellipse">
                    <figure class="category-media">
                        <a href="furniturelist.html">
                            <img src="{{asset('public/assets/front-end/cartKult/assets/images/list-3.jpg')}}" alt="Categroy" width="190" height="190" style="background-color: #9BC4CA;">
                        </a>
                    </figure>
                    <div class="category-content">
                        <h4 class="category-name">
                            <a href="furniturelist.html">Furniture</a>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="owl-item active" style="width: 86.6px; margin-right: 40px;">
                <div class="category category-ellipse">
                    <figure class="category-media">
                        <a href="electronicslist.html">
                            <img src="{{asset('public/assets/front-end/cartKult/assets/images/list-4.jpg')}}" alt="Categroy" width="190" height="190" style="background-color: #9BC4CA;">
                        </a>
                    </figure>
                    <div class="category-content">
                        <h4 class="category-name">
                            <a href="electronicslist.html">Electrolier</a>
                        </h4>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

        <!-- Start of Main -->
        <main class="main">
            <section class="intro-section ">
                <div class="owl-carousel owl-theme row gutter-no cols-1 animation-slider" data-owl-options="{
                    'nav': false,
                    'dots': true,
                    'autoplay': false,
                    'items': 1,
                    'dotsContainer': '.custom-dots'
                }">
                    <div class="banner banner-fixed content-center intro-slide intro-slide1" style="background-image: url(assets/images/b1.png); background-color: #EEF4F4;">
                        <div class="container">
                            <div class="banner-content d-inline-block y-50">
                                <div class="">
                                    <h5 class="banner-subtitle text-uppercase font-weight-bold">Deals and Promotions
                                    </h5>
                                    <h3 class="banner-title text-capitalize ls-25">
                                        <span class="text-primary">Winter Season</span><br> Fashion Lifestyle Collection
                                    </h3>
                                    <a href="fashionlist.html" class="btn btn-dark btn-outline btn-rounded btn-icon-right">
                                        Shop Now<i class="w-icon-long-arrow-right"></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- End of Intro Slide 1 -->
                    <div class="banner banner-fixed intro-slide intro-slide2" style="background-image: url(assets/images/b2.jpg); background-color: #EDEEF0;">
                        <div class="container">
                            <div class="banner-content d-inline-block y-50">
                                <div class="">
                                    <h5 class="banner-subtitle text-primary text-uppercase font-weight-bold mb-2">This Week Only!</h5>
                                    <h3 class="banner-title text-capitalize ls-25">Originals Comper Star. Shoes</h3>
                                    <hr class="banner-divider bg-dark">
                                    <p class="text-dark">Free Shipping on all orders over <strong>$80.00</strong></p>
                                    <a href="fashionlist.html" class="btn btn-dark btn-outline btn-rounded btn-icon-right">
                                        Shop Now<i class="w-icon-long-arrow-right"></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </section>
            <!-- End of Intro-section -->

            <div class="container" style="padding-top: 40px;">






                <!-- brand -->


                <h2 class="title title-underline mb-4 appear-animate">Top Vendors</h2>
                <div class="owl-carousel owl-theme row cols-lg-4 cols-md-3 cols-sm-2 cols-1 mb-10 pb-2 appear-animate" style="padding: 20px;" data-owl-options="{
                    'nav': false,
                    'dots': true,
                    'margin': 20,
                    'responsive': {
                        '0': {
                            'items': 1
                        },
                        '576': {
                            'items': 2
                        },
                        '768': {
                            'items': 3
                        },
                        '1200': {
                            'items': 4
                        }
                    }
                }">
                    <div class="vendor-widget mb-0">
                        <div class="vendor-widget-2">
                            <div class="vendor-details">
                                <figure class="vendor-logo">
                                    <a href="#">
                                        <img src="assets/images/demos/demo9/vendor-logo/4.jpg" alt="Vendor Logo" width="70" height="70" />
                                    </a>
                                </figure>
                                <div class="vendor-personal">
                                    <h4 class="vendor-name">
                                        <a href="#">Apple</a>
                                    </h4>
                                    <span class="vendor-product-count">(27 Products)</span>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width: 100%;"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vendor-products row cols-3 gutter-sm">
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="#">
                                            <img src="assets/images/demos/demo9/product/4-1.jpg" alt="Vendor Product" width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="#">
                                            <img src="assets/images/demos/demo9/product/4-2.jpg" alt="Vendor Product" width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="#">
                                            <img src="assets/images/demos/demo9/product/4-3.jpg" alt="Vendor Product" width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Vendor widget 2 -->
                    <div class="vendor-widget mb-0">
                        <div class="vendor-widget-2">
                            <div class="vendor-details">
                                <figure class="vendor-logo">
                                    <a href="#">
                                        <img src="assets/images/demos/demo9/vendor-logo/4.jpg" alt="Vendor Logo" width="70" height="70" />
                                    </a>
                                </figure>
                                <div class="vendor-personal">
                                    <h4 class="vendor-name">
                                        <a href="#">Apple</a>
                                    </h4>
                                    <span class="vendor-product-count">(20 Products)</span>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width: 100%;"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vendor-products row cols-3 gutter-sm">
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="#">
                                            <img src="assets/images/demos/demo9/product/4-4.jpg" alt="Vendor Product" width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="#">
                                            <img src="assets/images/demos/demo9/product/4-5.jpg" alt="Vendor Product" width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="#">
                                            <img src="assets/images/demos/demo9/product/4-6.jpg" alt="Vendor Product" width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Vendor widget 2 -->
                    <div class="vendor-widget mb-0">
                        <div class="vendor-widget-2">
                            <div class="vendor-details">
                                <figure class="vendor-logo">
                                    <a href="#">
                                        <img src="assets/images/demos/demo9/vendor-logo/4.jpg" alt="Vendor Logo" width="70" height="70" />
                                    </a>
                                </figure>
                                <div class="vendor-personal">
                                    <h4 class="vendor-name">
                                        <a href="#">Apple</a>
                                    </h4>
                                    <span class="vendor-product-count">(30 Products)</span>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width: 100%;"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vendor-products row cols-3 gutter-sm">
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-7.jpg" alt="Vendor Product" width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-8.jpg" alt="Vendor Product" width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-9.jpg" alt="Vendor Product" width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Vendor widget 2 -->
                    <div class="vendor-widget mb-0">
                        <div class="vendor-widget-2">
                            <div class="vendor-details">
                                <figure class="vendor-logo">
                                    <a href="vendor-dokan-store.html">
                                        <img src="assets/images/demos/demo9/vendor-logo/4.jpg" alt="Vendor Logo" width="70" height="70" />
                                    </a>
                                </figure>
                                <div class="vendor-personal">
                                    <h4 class="vendor-name">
                                        <a href="vendor-dokan-store.html">Apple</a>
                                    </h4>
                                    <span class="vendor-product-count">(17 Products)</span>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width: 100%;"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vendor-products row cols-3 gutter-sm">
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-10.jpg" alt="Vendor Product" width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-11.jpg" alt="Vendor Product" width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-12.jpg" alt="Vendor Product" width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Vendor widget 2 -->
                    <!-- End of Vendor widget 2 -->
                    <div class="vendor-widget mb-0">
                        <div class="vendor-widget-2">
                            <div class="vendor-details">
                                <figure class="vendor-logo">
                                    <a href="vendor-dokan-store.html">
                                        <img src="assets/images/demos/demo9/vendor-logo/4.jpg" alt="Vendor Logo" width="70" height="70" />
                                    </a>
                                </figure>
                                <div class="vendor-personal">
                                    <h4 class="vendor-name">
                                        <a href="vendor-dokan-store.html">Apple</a>
                                    </h4>
                                    <span class="vendor-product-count">(17 Products)</span>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width: 100%;"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vendor-products row cols-3 gutter-sm">
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-10.jpg" alt="Vendor Product" width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-11.jpg" alt="Vendor Product" width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-12.jpg" alt="Vendor Product" width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Vendor widget 2 -->
                    <!-- End of Vendor widget 2 -->
                    <div class="vendor-widget mb-0">
                        <div class="vendor-widget-2">
                            <div class="vendor-details">
                                <figure class="vendor-logo">
                                    <a href="vendor-dokan-store.html">
                                        <img src="assets/images/demos/demo9/vendor-logo/4.jpg" alt="Vendor Logo" width="70" height="70" />
                                    </a>
                                </figure>
                                <div class="vendor-personal">
                                    <h4 class="vendor-name">
                                        <a href="vendor-dokan-store.html">Apple</a>
                                    </h4>
                                    <span class="vendor-product-count">(17 Products)</span>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width: 100%;"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vendor-products row cols-3 gutter-sm">
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-10.jpg" alt="Vendor Product" width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-11.jpg" alt="Vendor Product" width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-12.jpg" alt="Vendor Product" width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Vendor widget 2 -->
                    <!-- End of Vendor widget 2 -->
                    <div class="vendor-widget mb-0">
                        <div class="vendor-widget-2">
                            <div class="vendor-details">
                                <figure class="vendor-logo">
                                    <a href="vendor-dokan-store.html">
                                        <img src="assets/images/demos/demo9/vendor-logo/4.jpg" alt="Vendor Logo" width="70" height="70" />
                                    </a>
                                </figure>
                                <div class="vendor-personal">
                                    <h4 class="vendor-name">
                                        <a href="vendor-dokan-store.html">Apple</a>
                                    </h4>
                                    <span class="vendor-product-count">(17 Products)</span>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width: 100%;"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vendor-products row cols-3 gutter-sm">
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-10.jpg" alt="Vendor Product" width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-11.jpg" alt="Vendor Product" width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                                <div class="vendor-product">
                                    <figure class="product-media">
                                        <a href="product-default.html">
                                            <img src="assets/images/demos/demo9/product/4-12.jpg" alt="Vendor Product" width="100" height="113" />
                                        </a>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Vendor widget 2 -->







                </div>
                <!-- End of Owl Carousel -->

                <!-- <div class="filter-with-title appear-animate">
                    <h2 class="title">Consumer Electronics</h2>
                    <ul class="nav-filters filter-boxed" data-target="#products-2">
                        <li><a href="#" class="nav-filter active" data-filter=".1-1">New Arrivals</a></li>
                        <li><a href="#" class="nav-filter" data-filter=".1-2">Best Seller</a></li>
                        <li><a href="#" class="nav-filter" data-filter=".1-3">Most Popular</a></li>
                        <li><a href="#" class="nav-filter" data-filter="*">View All</a></li>
                    </ul>
                </div> -->

                <!-- End of Iocn Box Wrapper -->




                <div class="category-banner-wrapper row grid appear-animate fadeIn appear-animation-visible" style="position: relative; height: 480px; animation-duration: 1.2s;">
                    <div class="grid-item col-lg-6 col-md-8 col-sm-7 height-x2" style="position: absolute; left: 0%; top: 0px;">
                        <div class="banner banner-fixed br-sm banner-lg">
                            <figure>
                                <img src="assets/images/1-1.jpg" alt="Banner" width="640" height="460" style="background-color: #F6F6F6;">
                            </figure>
                            <div class="banner-content y-50">
                                <h3 class="banner-title text-capitalize ls-25 mb-0">Season<br>Women Fashion
                                    <br>Collection</h3>
                                <p>Free shipping on all over <strong class="text-secondary">$99</strong></p>
                                <a href="fashionlist.html" class="btn btn-dark btn-outline btn-rounded btn-icon-right">
                                    Shop Now<i class="w-icon-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item col-lg-3 col-sm-6 height-x1" style="position: absolute; left: 49.9948%; top: 0px;">
                        <div class="banner banner-fixed br-sm banner-sm">
                            <figure>
                                <img src="assets/images/1-2.jpg" alt="Banner" width="310" height="220" style="background-color: #F2F2F2;">
                            </figure>
                            <div class="banner-content y-50 pt-2">
                                <h5 class="banner-subtitle font-weight-bold text-capitalize mb-2 ls-25">Best Seller</h5>
                                <h3 class="banner-title text-uppercase mb-0">Cosmetic</h3>
                                <div class="banner-price-info text-secondary text-capitalize ls-25">45% Off</div>
                                <a href="fashionlist.html" class="btn btn-dark btn-link btn-underline btn-icon-right">
                                    Shop Now<i class="w-icon-long-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item col-lg-3 col-md-4 col-sm-5 height-x2" style="position: absolute; left: 74.9922%; top: 0px;">
                        <div class="banner banner-fixed br-sm banner-md">
                            <figure>
                                <img src="assets/images/1-3.jpg" alt="Banner" width="310" height="220" style="background-color: #3D4753;">
                            </figure>
                            <div class="banner-content x-50 w-100 text-center">
                                <div class="banner-price-info text-white font-weight-bolder text-uppercase ls-25">40
                                    <sup class="font-weight-bold p-relative">%</sup>
                                    <sub class="font-weight-bold p-relative ls-normal">Off</sub>
                                </div>
                                <h3 class="banner-title text-uppercase text-white">Ultimate Sale</h3>
                                <p class="text-capitalize">Discount Selected Items</p>
                                <a href="furniturelist.html" class="btn btn-white btn-outline btn-rounded btn-icon-right">
                                    Shop Now<i class="w-icon-long-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item col-lg-3 col-sm-6 height-x1" style="position: absolute; left: 49.9948%; top: 240px;">
                        <div class="banner banner-fixed br-sm banner-sm">
                            <figure>
                                <img src="assets/images/1-4.jpg" alt="Banner" width="310" height="220" style="background-color: #414A59;">
                            </figure>
                            <div class="banner-content y-50 pt-2">
                                <h5 class="banner-subtitle text-white font-weight-bold text-capitalize mb-2 ls-25">
                                    Featured Event</h5>
                                <h3 class="banner-title text-white text-uppercase mb-0 ls-25">Electronics</h3>
                                <div class="banner-price-info text-primary text-capitalize ls-25">Sale</div>
                                <a href="electronicslist.html" class="btn btn-white btn-link btn-underline btn-icon-right">
                                    Shop Now<i class="w-icon-long-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="grid-space col-1" style="position: absolute; left: 0%; top: 480px;"></div>
                </div>







                <!-- gdrgdddddddddddddddd -->

                <div class="row grid grid-float pt-2 banner-grid mb-9 appear-animate">
                    <div class="grid-item col-lg-6 height-x2">
                        <div class="banner banner-fixed banner-lg br-sm">
                            <figure>
                                <img src="assets/images/demos/demo9/banner/1-1.jpg" alt="Banner" width="670" height="450" style="background-color: #E3E7EA;" />
                            </figure>
                            <div class="banner-content y-50">
                                <h5 class="banner-subtitle text-capitalize font-weight-normal mb-0 ls-25">
                                    Flash Sale <strong class="text-secondary text-uppercase">50% Off</strong>
                                </h5>
                                <h3 class="banner-title text-capitalize">Kitchen Collection</h3>
                                <p>Only until the end of this Week</p>
                                <a href="electronicslist.html" class="btn btn-dark btn-outline btn-rounded btn-icon-right">
                                    Shop Now<i class="w-icon-long-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item col-lg-6 height-x1">
                        <div class="banner banner-fixed banner-md br-sm">
                            <figure>
                                <img src="assets/images/demos/demo9/banner/1-2.jpg" alt="Banner" width="670" height="450" style="background-color: #2D2E32;" />
                            </figure>
                            <div class="banner-content">
                                <h3 class="banner-title text-white ls-25">
                                    Accessories<br><span class="font-weight-normal ls-normal">Collection</span>
                                </h3>
                                <a href="mobile_gadgetslist.html" class="btn btn-white btn-link btn-underline btn-icon-right">
                                    Shop Now<i class="w-icon-long-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item col-sm-6 col-lg-3 height-x1">
                        <div class="banner banner-fixed banner-sm br-sm">
                            <figure>
                                <img src="assets/images/demos/demo9/banner/1-3.jpg" alt="Banner" width="330" height="215" style="background-color: #181818;" />
                            </figure>
                            <div class="banner-content x-50 y-50 w-100 text-center">
                                <h3 class="banner-title font-secondary font-weight-normal mb-0 ls-25">Sale</h3>
                                <div class="banner-price-info font-weight-normal text-white mb-3">
                                    Up to <strong class="text-uppercase">20% Off</strong>
                                </div>
                                <a href="fashionlist.html" class="btn btn-white btn-link btn-underline">Shop
                                    Collection</a>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item col-sm-6 col-lg-3 height-x1">
                        <div class="banner banner-fixed banner-sm br-sm">
                            <figure>
                                <img src="assets/images/demos/demo9/banner/1-4.jpg" alt="Banner" width="330" height="215" style="background-color: #A3A8A6;" />
                            </figure>
                            <div class="banner-content">
                                <h5 class="banner-subtitle text-uppercase font-weight-bold">20% Off</h5>
                                <h3 class="banner-title text-capitalize ls-25">Kids Store</h3>
                                <a href="fashionlist" class="btn btn-dark btn-link btn-underline btn-icon-right">
                                    Shop Now<i class="w-icon-long-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Banner Grid -->


        </main>
        <!-- End of Main -->

 
@endsection

