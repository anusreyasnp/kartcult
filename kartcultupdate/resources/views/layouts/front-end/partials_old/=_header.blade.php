<style>
    .dropdown-menu {
        min-width: 304px !important;
        margin-left: -8px;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
    }

    .card-body.search-result-box {
        overflow: scroll;
        height: 400px;
        overflow-x: hidden;
    }

    .active .seller {
        font-weight: 700;
    }

    .for-count-value {
        position: absolute;

        right: 0.6875rem;;
        width: 1.25rem;
        height: 1.25rem;
        border-radius: 50%;
        color: "{{$web_config['primary_color']}}";

        font-size: .75rem;
        font-weight: 500;
        text-align: center;
        line-height: 1.25rem;
    }

    @media (min-width: 992px) {
        .navbar-sticky.navbar-stuck .navbar-stuck-menu.show {
            display: block;
            height: 55px !important;
        }
    }

    @media (min-width: 768px) {
        .navbar-stuck-menu {
            background-color: "{{$web_config['primary_color']}}";
            line-height: 15px;
            padding-bottom: 6px;
        }

    }

    @media (max-width: 767px) {
        .search_button {
            background-color: transparent !important;
        }

        .search_button .input-group-text i {
            color: "{{$web_config['primary_color']}}"!important;
        }

        .navbar-expand-md .dropdown-menu > .dropdown > .dropdown-toggle {
            position: relative;
            padding-right: 1.95rem;
        }

        .navbar-brand img {

        }

        .mega-nav1 {
            background: white;
            color: "{{$web_config['primary_color']}}"!important;
            border-radius: 3px;
        }

        .mega-nav1 .nav-link {
            color: "{{$web_config['primary_color']}}"!important;
        }
    }

    @media (max-width: 768px) {
        .tab-logo {
            width: 10rem;
        }
    }

    @media (max-width: 360px) {
        .mobile-head {
            padding: 3px;
        }
    }

    @media (max-width: 471px) {
        .navbar-brand img {

        }

        .mega-nav1 {
            background: white;
            color: "{{$web_config['primary_color']}}"!important;
            border-radius: 3px;
        }

        .mega-nav1 .nav-link {
            color: "{{$web_config['primary_color']}}"!important;
        }
    }


</style>


<header class="header">

    <!-- End of Header Top -->

    <div class="header-middle sticky-content fix-top sticky-header" id="myHeader-fix">
        <div class="container">
            <div class="header-left mr-md-4">

                <a href="demo-2.html" class="logo ml-lg-0">
                    <img src="https://demo.esterweb.in/kartkult/k22/assets/images/kartcult-2.png" alt="logo" width="144" height="45" />
                </a>
                <div class="header-call d-xs-show d-lg-flex align-items-center">
                    <a href="tel:#" class="w-icon-map-marker" style="
                    font-size: 24px;
                "></a>
                    <div class="call-info d-lg-show">
                        <h4 class="chat font-weight-normal font-size-md text-normal ls-normal text-light mb-0">

                            <a href="tel:#" class="phone-number font-weight-bolder ls-50">Thrissur kerala</a>
                    </div>
                </div>
                <form method="get" action="#" class="input-wrapper header-search hs-expanded hs-round d-none d-md-flex">
                    <div class="select-box">
                        <select id="category" name="category">
                            <option value="">All Categories</option>
                            <option value="4">Fashion</option>
                            <option value="5">Furniture</option>
                            <option value="6">Shoes</option>
                            <option value="7">Sports</option>
                            <option value="8">Games</option>
                            <option value="9">Computers</option>
                            <option value="10">Electronics</option>
                            <option value="11">Kitchen</option>
                            <option value="12">Clothing</option>
                        </select>
                    </div>
                    <input type="text" class="form-control" name="search" id="search" placeholder="Search in..." required />
                    <button class="btn btn-search" type="submit"><i class="w-icon-search"></i>
                    </button>
                </form>
            </div>
            <div class="header-right ml-4">
                <a href="login.html" class="mobile-menu-toggle  w-icon-hamburger">
                </a>
                <a class="wishlist label-down link d-xs-show" href="login.html">
                    <i class="w-icon-account"></i>
                    <span class="wishlist-label d-lg-show">login</span>
                </a>
                <a class="wishlist label-down link d-xs-show" href="wishlist.html">
                    <i class="w-icon-heart"></i>
                    <span class="wishlist-label d-lg-show">wishlist</span>
                </a>

                <a class="compare label-down link d-xs-show" href="compare.html">
                    <i class="w-icon-compare"></i>
                    <span class="compare-label d-lg-show">credit points</span>
                </a>
                <div class="dropdown cart-dropdown cart-offcanvas mr-0 mr-lg-2">
                    <div class="cart-overlay"></div>
                    <a href="cart.html" class="cart-toggle label-down link">
                        <i class="w-icon-cart">
                            <span class="cart-count">2</span>
                        </i>
                        <span class="cart-label">Cart</span>
                    </a>

                    <!-- End of Dropdown Box -->
                </div>
            </div>
        </div>
    </div>
    <!-- End of Header Middle -->



   
    <!-- End of Container -->
</header>

