<style>
    .dropdown-menu {
        min-width: 251px !important;
        margin-left: -8px;
        border-top-left-radius: .1px;
        border-top-right-radius: .1px;
    }

    .card-body.search-result-box {
        overflow: scroll;
        height: 400px;
        overflow-x: hidden;
    }

    .seller {
        font-weight: 600;
    }

    .active .seller {
        font-weight: 700;
    }

    .for-count-value {
        position: absolute;

        right: 0.6875rem;;
        width: 1.25rem;
        height: 1.25rem;
        border-radius: 50%;
        color: {{$web_config['primary_color']}};

        font-size: .75rem;
        font-weight: 500;
        text-align: center;
        line-height: 1.25rem;
    }

    @media (min-width: 992px) {
        .navbar-sticky.navbar-stuck .navbar-stuck-menu.show {
            display: block;
            height: 55px !important;
        }
    }

    @media (min-width: 768px) {
        .navbar-stuck-menu {
            background-color: {{$web_config['primary_color']}};
            line-height: 15px;
            padding-bottom: 6px;
        }

        .web {
            display: block;
        }

        .mobile {
            display: none;
        }
    }

    @media (max-width: 767px) {
        .search_button {
            background-color: transparent !important;
        }

        .search_button .input-group-text i {
            color: {{$web_config['primary_color']}}            !important;
        }

        .navbar-expand-md .dropdown-menu > .dropdown > .dropdown-toggle {
            position: relative;
            padding-right: 1.95rem;
        }

        .navbar-brand img {

        }

        .web {
            display: none;
        }

        .mobile {
            display: block;
        }

        .mega-nav1 {
            background: white;
            color: {{$web_config['primary_color']}}            !important;
            border-radius: 3px;
        }

        .mega-nav1 .nav-link {
            color: {{$web_config['primary_color']}}            !important;
        }
    }

    @media (max-width: 768px) {
        .tab-logo {
            width: 10rem;
        }
    }

    @media (max-width: 360px) {
        .mobile-head {
            padding: 3px;
        }
    }

    @media (max-width: 471px) {
        .navbar-brand img {

        }

        .web {
            display: none !important;
        }

        .mobile {
            display: block !important;
        }

    }


</style>

        <!-- Start of Header -->
        <header class="header">

            <!-- End of Header Top -->

            @php
            $locale = session()->get('locale') ;
            if ($locale==""){
                $locale = "en";
            }
            \App\CPU\Helpers::currency_load();
            $currency_code = session('currency_code');
            $currency_symbol= session('currency_symbol');
            if ($currency_symbol=="")
            {
                $system_default_currency_info = \session('system_default_currency_info');
                $currency_symbol = $system_default_currency_info->symbol;
                $currency_code = $system_default_currency_info->code;
            }
            $language=\App\CPU\Helpers::language_load();
            $company_phone =$web_config['phone']->value;
            $company_name =$web_config['name']->value;
            $company_web_logo =$web_config['web_logo']->value;
            $company_mobile_logo =$web_config['mob_logo']->value;
        @endphp


            <div class="header-middle sticky-content fix-top sticky-header" id="myHeader-fix">
                <div class="container">
                    <div class="header-left mr-md-4">

                        <a href="{{route('home')}}" class="logo ml-lg-0">
                            <img width="150" height="45" 
                            src="https://demo.esterweb.in/kartkult/k22/assets/images/kartcult-2.png">
                            {{-- "{{asset("storage/app/public/company/$company_web_logo")}}"
                            onerror="this.src='{{asset('public/assets/front-end/img/image-place-holder.png')}}'"
                            alt="{{$company_name}}"/> --}}
                        </a>
                        <div class="header-call d-xs-show d-lg-flex align-items-center">
                            <a href="tel:#" class="w-icon-map-marker" style="
                            font-size: 24px;
                        "></a>
                            <div class="call-info d-lg-show">
                                <h4 class="chat font-weight-normal font-size-md text-normal ls-normal text-light mb-0">

                                    <a href="tel:#" class="phone-number font-weight-bolder ls-50">Thrissur </a>
                            </div>
                        </div>
                        <form type="submit" action="{{route('products')}}" class="input-wrapper header-search hs-expanded hs-round d-none d-md-flex search_form">
                            <div class="select-box">
                                @php($categories=\App\CPU\CategoryManager::parents())
                                <select id="category" name="category">
                                    <option value="">All Categories</option>
                                    @foreach($categories as $category)
                                        <option value="4">{{$category['name']}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <input class="form-control appended-form-control search-bar-input-mobile" type="text" autocomplete="off" placeholder="{{trans('messages.search')}}" name="name">

                            <button class="btn btn-search" type="submit"><i class="w-icon-search"></i>
                            </button>
                        </form>
                    </div>
                    <div class="header-right ml-4">
                        {{-- <a href="login.html" class="mobile-menu-toggle  w-icon-hamburger">
                        </a> --}}
                        @if(!auth('customer')->check())
                            <a class="wishlist label-down link d-xs-show" href="{{route('customer.auth.login')}}">
                                <i class="w-icon-account"></i>
                                <span class="wishlist-label d-lg-show">login</span>
                            </a>
                        @else
                            <a class="wishlist label-down link d-xs-show" href="{{route('user-account')}}">
                                <i class="w-icon-account"></i>
                                <span class="wishlist-label d-lg-show">
                                    Hello, {{auth('customer')->user()->f_name}} {{auth('customer')->user()->l_name}}</span>
                            </a>
                        @endif

                        <a class="wishlist label-down link d-xs-show" href="{{route('wishlists')}}">
                            <i class="w-icon-heart"></i>
                            <span class="wishlist-label d-lg-show">wishlist</span>
                        </a>

                        <a class="compare label-down link d-xs-show" >
                            <i class="w-icon-compare"></i>
                            <span class="compare-label d-lg-show">credit points</span>
                        </a>
                        <div class="dropdown cart-dropdown cart-offcanvas mr-0 mr-lg-2">

                            
                            <div class="cart-overlay"></div>
                            <a href="{{route('shop-cart')}}" class="cart-toggle label-down link">
                                <i class="w-icon-cart">
                                    <span class="cart-count"> {{session()->has('cart')?count(session()->get('cart')):0}} </span>
                                </i>
                                <span class="cart-label">Cart</span>
                            </a>

                            <!-- End of Dropdown Box -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of Header Middle -->

            @php($categories=\App\CPU\CategoryManager::parents())
            <div class="category-wrapper owl-carousel owl-theme appear-animate owl-loaded owl-drag fadeIn appear-animation-visible" style="padding: 12px;animation-duration: 1.2s;background: #f0f0f0;" data-owl-options="{
                'nav': false,
                'dots': false,
                'margin': 34,
                'responsive': {
                    '0': {
                        'items': 4
                    },
                    '576': {
                        'items': 5
                    },
                    '768': {
                        'items': 6
                    },
                    '992': {
                        'items': 8
                    },
                    '1200': {
                        'items': 10
                    }
                }
            }">

                <div class="owl-stage-outer">
                    <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0.7s ease 0s; width: 1920px;">
                        @foreach($categories as $category)

                            <div class="owl-item active" style="width: 86.6px; margin-right: 40px;">
                                <div class="category category-ellipse">
                                    <figure class="category-media">
                                    <a href="{{route('products',['id'=> $category['id'],'data_from'=>'category','page'=>1])}}">
                                            <img src="{{asset("storage/app/public/category/$category->icon")}}"
                                            onerror="this.src='{{asset('public/assets/front-end/img/image-place-holder.png')}}'" alt="Categroy" width="200" height="200" style="background-color: #9BC4CA;">
                                    </a>
                                    </figure>
                                    <div class="category-content">
                                        <h4 class="category-name">
                                            <a href="{{route('products',['id'=> $category['id'],'data_from'=>'category','page'=>1])}}">{{$category['name']}}</a>
                                        </h4>
                                    </div>
                                </div>
                            </div>

                        @endforeach


                      
                    </div>
                </div>

            </div>

        </header>
        <!-- End of Header -->

