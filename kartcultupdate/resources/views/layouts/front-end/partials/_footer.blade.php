<!-- Footer -->
<style>
    .page-footer {
        background: #333e4f;
        color: white;
    }

    .social-btn {
        border: 1px solid white;
        border-radius: 50%;
        height: 2rem;
        width: 2rem;
    }

    .btn-market .btn-market-title {
        font-size: 14px !important;
    }

    .social-btn i {
        line-height: 1.90rem;
    }

    .compny_name {
        font-size: 39px;
        margin-left: 5px;
    }

    .for-margin {
        margin-top: 10px;
    }

    .font-weight-bold {
        font-weight: 600 !important;
    }

    .footer-heder {
        color: #FFFFFF;
    }


    .payment-card {
        background: white;
        border-radius: 6px;
        max-width: 134px;
        padding-bottom: 5px;
        padding-top: 5px;
        margin-bottom: 3px;
        margin-right: 10px;
        margin-left: 5px;
    }

    .fa-shopping-cart {
        font-size: 56px;

    }

    .widget-list-link {
        color: #d9dce2;
    }

    .page-footer hr {
        border: 0.001px solid #2d3542;
    }
    .social-media :hover{
     color:  {{$web_config['secondary_color']}} !important;
    }
    @media (min-width: 768px) {
        .fa-shopping-cart {
            font-size: 48px;

        }


        .compny_name {
            font-size: 30px;
            margin-left: 3px;
        }
    }
    @media(max-width:1024px){
        .payment-tab{
            display: none;
        }
    }
 @media(max-width: 768px){
    .payment-tab{
            display: none;
        }
        .apple_app{
            padding-right: 0px;
        }
       .apple_app{
           padding-right: 0% !important;
       }
       .goole_app{
           padding-left: 0% !important;
       }
       .razorpay{
        margin-left: 30%;
    margin-top: 7px;
       }
 }
    @media (max-width: 500px) {
        .razorpay{
        margin-left: 30%;
    margin-top: 7px;
       }
        .mobile-padding {
            margin-bottom: 4%;
        }

        .widget-list {
            margin-bottom: 3%;
        }

        .for-mobile-delivery {
            margin-left: 15px;
        }

    }
    @media(max-width: 360px){
        .glaxy-for-mobile{
            margin-left: 1rem;
    margin-bottom: 0.55rem;
        }
    }
    @media(max-width: 375px){
        .razorpay{
        margin-left: 31%;
    margin-top: 7px;
       }
        .glaxy-for-mobile{
            margin-left: 1rem;
    margin-bottom: 0.55rem;
        }
    }
    @media(max-width: 414px)
    {
        .glaxy-for-mobile{
            margin-left: 26px;
    margin-bottom: 10px;
        }
    }
    @media(max-width: 425px)
    {
        .glaxy-for-mobile{
            margin-left: 26px;
    margin-bottom: 10px;
        }
    }

    @media screen and (max-width: 500px) {
        .title_message {
            visibility: hidden;
            clear: both;
            float: left;
            margin: 10px auto 5px 20px;
            width: 28%;
            display: none;
        }
        .for-m-p{
            margin-bottom: 5px;
    margin-left: 8%;
        }
    }
</style>

     <!-- Start of Footer -->
     <footer class="footer footer-dark ">
        <div class="footer-newsletter bg-primary">
            <div class="container">
                <div class="row justify-content-center align-items-center">
                    <div class="col-xl-5 col-lg-6">
                        <div class="icon-box icon-box-side text-white">
                            <div class="icon-box-icon d-inline-flex">
                                <i class="w-icon-envelop3"></i>
                            </div>
                            <div class="icon-box-content">
                                <h4 class="icon-box-title text-white text-uppercase font-weight-bold">Subscribe To Our Newsletter</h4>
                                <p class="text-white">Get all the latest information on Events, Sales and Offers.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-6 col-md-9 mt-4 mt-lg-0 ">
                        <form action="#" method="get" class="input-wrapper input-wrapper-inline input-wrapper-rounded">
                            <input type="email" class="form-control mr-2 bg-white" name="email" id="email" placeholder="Your E-mail Address" />
                            <button class="btn btn-dark btn-rounded" type="submit">Subscribe<i
                                    class="w-icon-long-arrow-right"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="footer-top">
                <div class="row">
                    <div class="col-lg-4 col-sm-6">
                        <div class="widget widget-about">
                            <a href="demo9.html" class="logo-footer">
                                <img width="144" height="45" 
                                src="https://demo.esterweb.in/kartkult/k22/assets/images/kartcult-2.png">
                                {{-- "{{asset("storage/app/public/company/")}}/{{ $web_config['footer_logo']->value }}"
                                onerror="this.src='{{asset('public/assets/front-end/img/image-place-holder.png')}}'"
                                alt="{{ $web_config['name']->value }}"/> --}}
                            </a>
                            <div class="widget-body">
                                <p class="widget-about-title">Got Question? Call us 24/7</p>
                                <a href="tel:18005707777" class="widget-about-call">1-800-570-7777</a>
                                <p class="widget-about-desc">Register now to get updates on pronot get up icons & coupons ster now toon.
                                </p>

                            @php
                                $social_media = \App\Model\SocialMedia::where('active_status', 1)->get();
                            @endphp
                        
                                <div class="social-icons social-icons-colored">
                                    @if(isset($social_media))
                                       @foreach ($social_media as $item)
                                          <a href="{{$item->link}}" class="social-icon social-{{$item->name}} w-icon-{{$item->name}}"></a>
                                       @endforeach
                                    @endif
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="widget">

                            <h3 class="widget-title">{{trans('messages.about_us')}}</h3>
                            <ul class="widget-body">
                                <li class="widget-list-item">
                                    <a class="widget-list-link" href="{{route('about-us')}}">{{trans('messages.about_company')}}</a>
                                </li>
                                <li class="widget-list-item">
                                    <a class="widget-list-link" href="{{route('helpTopic')}}">{{trans('messages.faq')}}</a>
                                </li>
                                <li class="widget-list-item ">
                                    <a class="widget-list-link" href="{{route('terms')}}">{{trans('messages.terms_&_conditions')}}</a>
                                 </li>
                                <li class="widget-list-item ">
                                    <a class="widget-list-link" href="{{route('privacy-policy')}}">
                                        {{trans('messages.privacy_policy')}}
                                    </a>
                                </li>
                                <li class="widget-list-item ">
                                    <a class="widget-list-link" href="{{route('contacts')}}">{{trans('messages.contact_us')}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="widget">
                            <h4 class="widget-title">My Account</h4>
                            <ul class="widget-body">
                                <li class="widget-list-item"><a class="widget-list-link" href="{{route('track-order.index')}}">{{trans('messages.track_order')}}</a></li>
                                <li class="widget-list-item"><a class="widget-list-link" href="{{route('shop-cart')}}">View Cart</a></li>
                                @if(!auth('customer')->check())
                                   <li class="widget-list-item"><a class="widget-list-link"href="{{route('customer.auth.login')}}">Sign In</a></li>
                                @else 
                                   <li class="widget-list-item"><a class="widget-list-link"href="{{route('user-account')}}">My Account</a></li>
                                @endif
                                <li class="widget-list-item"><a class="widget-list-link" href="{{route('helpTopic')}}">{{trans('messages.faq')}}</a></li>
                                <li class="widget-list-item"><a class="widget-list-link" href="{{route('wishlists')}}">{{trans('messages.wish_list')}}</a></li>
                                <li class="widget-list-item"><a class="widget-list-link" href="{{route('privacy-policy')}}">{{trans('messages.privacy_policy')}}</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="widget">
                            <h4 class="widget-title">Customer Service</h4>
                            <ul class="widget-body">
                                <li class="widget-list-item"><a class="widget-list-link" href="#">Payment Methods</a></li>
                                <li class="widget-list-item"><a class="widget-list-link" href="#">Money-back guarantee!</a></li>
                                <li class="widget-list-item"><a class="widget-list-link" href="#">Product Returns</a></li>
                                <li class="widget-list-item"><a class="widget-list-link" href="{{ route('account-tickets') }}">Support Center</a></li>
                                <li class="widget-list-item"><a class="widget-list-link" href="#">Shipping</a></li>
                                <li class="widget-list-item"><a class="widget-list-link" href="{{route('terms')}}">{{trans('messages.terms_&_conditions')}}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer-bottom">
                <div class="footer-left">
                    <p class="copyright">{{ $web_config['copyright_text']->value }}</p>
                </div>
                <div class="footer-right">
                </div>
            </div>
        </div>
    </footer>
    <!-- End of Footer -->

